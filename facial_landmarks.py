from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

def ear(eyes):
	eye_EAR = dst(eyes[1],eyes[5])+dst(eyes[2],eyes[4]) / 2*dst(eyes[0],eyes[3])
	return eye_EAR

# The function is used for calculating the distance between the two points. This is primarily used for calculating EAR
def dst(point1, point2):
	distance = np.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)
	return distance

def PolyArea(pts):
	lines = np.hstack([pts,np.roll(pts,-1,axis=0)])
	area = 0.5*abs(sum(x1*y2-x2*y1 for x1,y1,x2,y2 in lines))
	return area
# construct the argument parser and parse the arguments

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('/home/rohit/test/facial-landmarks/shape_predictor_68_face_landmarks.dat')
cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 18.0, (640,480))
# load the input image, resize it, and convert it to grayscale
ears = []
b = 0

while(True):
	try:
		_,image = cap.read()
		# image = imutils.resize(image, width=320)
		# print(image.shape)
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		# detect faces in the grayscale image
		rects = detector(gray, 1)

		# loop over the face detections
		for (i, rect) in enumerate(rects):
			# determine the facial landmarks for the face region, then
			# convert the facial landmark (x, y)-coordinates to a NumPy
			# array
			shape = predictor(gray, rect)
			shape = face_utils.shape_to_np(shape)

			# convert dlib's rectangle to a OpenCV-style bounding box
			# [i.e., (x, y, w, h)], then draw the face bounding box
			(x, y, w, h) = face_utils.rect_to_bb(rect)
			cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

			# show the face number
			# cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
			# 	cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

			# loop over the (x, y)-coordinates for the facial landmarks
			# and draw them on the image
			for (x, y) in shape:
				cv2.circle(image, (x, y), 1, (0, 0, 255), -1)

		cv2.putText(image, "Blink count: {}".format(b), (50,50),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
		eye_left = shape[36:42]
		eye_right = shape[42:48]
		l,r = ear(eye_left), ear(eye_right)
		ear_avg = (l+r)/2
		ears.append(ear_avg)
		ear_smooth = gaussian_filter(ears,sigma = 5)
		# set blink threshold
		if ear_avg < 100:
			b+=1
			print('real person')
		out.write(image)
		cv2.imshow("Output", image)
		# show the output image with the face detections + facial landmarks
		if cv2.waitKey(1) & 0xFF == ord('q'):
			plt.plot(ears)
			plt.show()
			break
	except Exception as e:
		print(e)
	
cap.release()
out.release()
cv2.destroyAllWindows()	